import * as express from "express";
import * as productController from '../controller/ProductController';
const router = express.Router();


router.get('/all', productController.all);


export default router;