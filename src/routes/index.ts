import * as express from 'express';

import productRoute from './product.route';

const router = express.Router();

router.use('/api/product', productRoute);

export default router;